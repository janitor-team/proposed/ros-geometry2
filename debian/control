Source: ros-geometry2
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Jochen Sprickerhof <jspricke@debian.org>,
           Leopold Palomo-Avellaneda <leo@alaxarxa.net>,
           Timo Röhling <roehling@debian.org>,
Build-Depends: debhelper-compat (= 13),
		catkin (>= 0.8.10-1~), libroscpp-core-dev, ros-message-generation, libstd-msgs-dev,
		python3-all-dev, python3-setuptools, dh-sequence-python3,
		libgeometry-msgs-dev, ros-actionlib-msgs,
		libconsole-bridge-dev, python3-rospy (>= 1.14.3+ds1-7), python3-rosgraph (>= 1.14.3+ds1-7),
		libactionlib-dev, librosconsole-dev,
		libros-rosgraph-msgs-dev, libxmlrpcpp-dev,
		libmessage-filters-dev, ros-cmake-modules, libeigen3-dev,
		liborocos-kdl-dev, libsensor-msgs-dev, libbullet-dev,
		libboost-dev, libboost-thread-dev,
		libboost-filesystem-dev, libboost-regex-dev,
		libroscpp-dev, ros-sensor-msgs, librostest-dev, libgtest-dev,
		python3-rostest <!nocheck>, python3-sensor-msgs <!nocheck>,
		python3-pykdl <!nocheck>, python3-actionlib <!nocheck>,
Standards-Version: 4.6.1
Section: libs
Rules-Requires-Root: no
Homepage: https://wiki.ros.org/geometry2
Vcs-Browser: https://salsa.debian.org/science-team/ros-geometry2
Vcs-Git: https://salsa.debian.org/science-team/ros-geometry2.git

Package: libtf2-2d
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Multi-Arch: same
Description: Robot OS tf2 transform library
 This package is part of Robot OS (ROS). tf2 is the second generation
 of the transform library, which lets the user keep track of multiple
 coordinate frames over time. tf2 maintains the relationship between
 coordinate frames in a tree structure buffered in time, and lets the
 user transform points, vectors, etc between any two coordinate frames
 at any desired point in time.
 .
 This package contains the library itself.

Package: libtf2-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, libtf2-2d ( = ${binary:Version}), libconsole-bridge-dev, libboost-dev, libgeometry-msgs-dev, libroscpp-core-dev, libtf2-msgs-dev, libtf2-geometry-msgs-dev,
Description: Robot OS tf2 transform library - development files
 This package is part of Robot OS (ROS). tf2 is the second generation
 of the transform library, which lets the user keep track of multiple
 coordinate frames over time. tf2 maintains the relationship between
 coordinate frames in a tree structure buffered in time, and lets the
 user transform points, vectors, etc between any two coordinate frames
 at any desired point in time.
 .
 This package contains the development files of the tf2 library.

Package: python3-tf2
Section: python
Architecture: any
Depends: ${python3:Depends}, ${misc:Depends}, ${shlibs:Depends}, python3-rospy, libtf2-dev, python3-geometry-msgs
Description: Robot OS tf2 transform library - Python 3
 This package is part of Robot OS (ROS). tf2 is the second generation
 of the transform library, which lets the user keep track of multiple
 coordinate frames over time. tf2 maintains the relationship between
 coordinate frames in a tree structure buffered in time, and lets the
 user transform points, vectors, etc between any two coordinate frames
 at any desired point in time.
 .
 This package contains the Python 3 binding.

Package: libtf2-ros1d
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Multi-Arch: same
Description: Robot OS binding for tf2 transform library
 This package is part of Robot OS (ROS). tf2 is the second generation
 of the transform library, which lets the user keep track of multiple
 coordinate frames over time. tf2 maintains the relationship between
 coordinate frames in a tree structure buffered in time, and lets the
 user transform points, vectors, etc between any two coordinate frames
 at any desired point in time.
 .
 This package contains the ROS bindings to tf2.

Package: libtf2-ros-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, libtf2-dev, libtf2-ros1d (= ${binary:Version}), libactionlib-dev, libactionlib-msgs-dev, libgeometry-msgs-dev, libmessage-filters-dev, libroscpp-dev, libtf2-msgs-dev,
Description: Robot OS binding for tf2 transform library - dev files
 This package is part of Robot OS (ROS). tf2 is the second generation
 of the transform library, which lets the user keep track of multiple
 coordinate frames over time. tf2 maintains the relationship between
 coordinate frames in a tree structure buffered in time, and lets the
 user transform points, vectors, etc between any two coordinate frames
 at any desired point in time.
 .
 This package contains the development files of the ROS binding to tf2.

Package: python3-tf2-ros
Section: python
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}, python3-rospy, python3-std-msgs, python3-actionlib, libtf2-ros-dev, python3-tf2-msgs, python3-tf2, python3-rosgraph, python3-geometry-msgs
Description: Robot OS binding for tf2 transform library - Python 3
 This package is part of Robot OS (ROS). tf2 is the second generation
 of the transform library, which lets the user keep track of multiple
 coordinate frames over time. tf2 maintains the relationship between
 coordinate frames in a tree structure buffered in time, and lets the
 user transform points, vectors, etc between any two coordinate frames
 at any desired point in time.
 .
 This package contains the ROS Python 3 binding to tf2.

Package: ros-tf2-msgs
Section: devel
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ros-geometry-msgs, ros-actionlib-msgs
Description: Robot OS messages for tf2 transform library - definitions
 This package is part of Robot OS (ROS). tf2 is the second generation
 of the transform library, which lets the user keep track of multiple
 coordinate frames over time. tf2 maintains the relationship between
 coordinate frames in a tree structure buffered in time, and lets the
 user transform points, vectors, etc between any two coordinate frames
 at any desired point in time.
 .
 This package contains the message definitions.

Package: libtf2-msgs-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ros-message-generation, libgeometry-msgs-dev, libactionlib-msgs-dev
Description: Robot OS messages for tf2 transform library - dev files
 This package is part of Robot OS (ROS). tf2 is the second generation
 of the transform library, which lets the user keep track of multiple
 coordinate frames over time. tf2 maintains the relationship between
 coordinate frames in a tree structure buffered in time, and lets the
 user transform points, vectors, etc between any two coordinate frames
 at any desired point in time.
 .
 This package contains the development files.

Package: python3-tf2-msgs
Section: python
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}, python3-geometry-msgs, python3-actionlib-msgs
Description: Robot OS messages for tf2 transform library - Python 3
 This package is part of Robot OS (ROS). tf2 is the second generation
 of the transform library, which lets the user keep track of multiple
 coordinate frames over time. tf2 maintains the relationship between
 coordinate frames in a tree structure buffered in time, and lets the
 user transform points, vectors, etc between any two coordinate frames
 at any desired point in time.
 .
 This package contains the Python 3 code.

Package: cl-tf2-msgs
Section: lisp
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, cl-geometry-msgs, cl-actionlib-msgs
Description: Robot OS tf2 transform library messages - LISP
 This package is part of Robot OS (ROS). tf2 is the second generation
 of the transform library, which lets the user keep track of multiple
 coordinate frames over time. tf2 maintains the relationship between
 coordinate frames in a tree structure buffered in time, and lets the
 user transform points, vectors, etc between any two coordinate frames
 at any desired point in time.
 .
 This package contains the LISP binding (messages).

Package: cl-tf2-srvs
Section: lisp
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: Robot OS tf2 transform library services - LISP
 This package is part of Robot OS (ROS). tf2 is the second generation
 of the transform library, which lets the user keep track of multiple
 coordinate frames over time. tf2 maintains the relationship between
 coordinate frames in a tree structure buffered in time, and lets the
 user transform points, vectors, etc between any two coordinate frames
 at any desired point in time.
 .
 This package contains the LISP binding (services).


Package: libtf2-sensor-msgs-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, libeigen3-dev, libsensor-msgs-dev, libtf2-ros-dev, libtf2-dev
Description: Small lib for ROS to transform sensor_msgs with tf2 - dev files
 This package is part of Robot OS (ROS). tf2 is the second generation
 of the transform library, which lets the user keep track of multiple
 coordinate frames over time. tf2 maintains the relationship between
 coordinate frames in a tree structure buffered in time, and lets the
 user transform points, vectors, etc between any two coordinate frames
 at any desired point in time.
 .
 This package contains utils to transform sensor_msgs with tf2.

Package: python3-tf2-sensor-msgs
Section: python
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}, python3-sensor-msgs, python3-pykdl, python3-rospy, python3-tf2-ros
Description: Robot OS sensor messages for tf2 transform library - Python 3
 This package is part of Robot OS (ROS). tf2 is the second generation
 of the transform library, which lets the user keep track of multiple
 coordinate frames over time. tf2 maintains the relationship between
 coordinate frames in a tree structure buffered in time, and lets the
 user transform points, vectors, etc between any two coordinate frames
 at any desired point in time.
 .
 This package contains the Python 3 code to transform sensor_msgs with tf2.

Package: libtf2-bullet-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, libgeometry-msgs-dev, libtf2-dev, libbullet-dev,
Description: Robot OS tf2 transform library for Bullet - dev files
 This package is part of Robot OS (ROS). tf2 is the second generation
 of the transform library, which lets the user keep track of multiple
 coordinate frames over time. tf2 maintains the relationship between
 coordinate frames in a tree structure buffered in time, and lets the
 user transform points, vectors, etc between any two coordinate frames
 at any desired point in time.
 .
 This package contains the development files for libtf2 for Bullet.

#tf2_eigen
Package: libtf2-eigen-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, libeigen3-dev, libgeometry-msgs-dev, libtf2-dev
Description: Robot OS tf2 transform library for Eigen - dev files
 This package is part of Robot OS (ROS). tf2 is the second generation
 of the transform library, which lets the user keep track of multiple
 coordinate frames over time. tf2 maintains the relationship between
 coordinate frames in a tree structure buffered in time, and lets the
 user transform points, vectors, etc between any two coordinate frames
 at any desired point in time.
 .
 This package contains development files to convert between libtf2 and Eigen.

#tf2_geometry_msgs
Package: libtf2-geometry-msgs-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: liborocos-kdl-dev, ${misc:Depends}, libgeometry-msgs-dev, libtf2-dev,
Description: Robot OS geometry messages for tf2 transform library - dev files
 This package is part of Robot OS (ROS). tf2 is the second generation
 of the transform library, which lets the user keep track of multiple
 coordinate frames over time. tf2 maintains the relationship between
 coordinate frames in a tree structure buffered in time, and lets the
 user transform points, vectors, etc between any two coordinate frames
 at any desired point in time.
 .
 This package contains the development files to transform geometry messages
 to tf2.

Package: python3-tf2-geometry-msgs
Section: python
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}, python3-geometry-msgs, python3-pykdl, python3-rospy, python3-tf2-ros
Description: Robot OS tf2 transform library for Bullet - Python 3
 This package is part of Robot OS (ROS). tf2 is the second generation
 of the transform library, which lets the user keep track of multiple
 coordinate frames over time. tf2 maintains the relationship between
 coordinate frames in a tree structure buffered in time, and lets the
 user transform points, vectors, etc between any two coordinate frames
 at any desired point in time.
 .
 This package contains the Python 3 code to transform geometry messages to tf2.

#tf2_kdl
Package: libtf2-kdl-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, liborocos-kdl-dev, libtf2-dev, libtf2-ros-dev, libtf2-msgs-dev, libeigen3-dev
Description: Robot OS tf2 transform library using Orocos-KDL - dev files
 This package is part of Robot OS (ROS). tf2 is the second generation
 of the transform library, which lets the user keep track of multiple
 coordinate frames over time. tf2 maintains the relationship between
 coordinate frames in a tree structure buffered in time, and lets the
 user transform points, vectors, etc between any two coordinate frames
 at any desired point in time.
 .
 This package contains the development files to convert between tf2 and
 liborocos-kdl.

Package: python3-tf2-kdl
Section: python
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}, python3-pykdl, python3-rospy, python3-geometry-msgs, python3-tf2-ros
Description: Robot OS tf2 transform library using Orocos-KDL - Python 3
 This package is part of Robot OS (ROS). tf2 is the second generation
 of the transform library, which lets the user keep track of multiple
 coordinate frames over time. tf2 maintains the relationship between
 coordinate frames in a tree structure buffered in time, and lets the
 user transform points, vectors, etc between any two coordinate frames
 at any desired point in time.
 .
 This package contains the Python 3 code to convert betweenw tf2 and
 liborocos-kdl.

Package: tf2-tools
Section: python
Architecture: any
# explicitly depend on python3:any because dh_python3 doesn't pick it up from
# /usr/lib/tf2_tools/view_frames.py
Depends: ${python3:Depends}, ${shlibs:Depends}, ${misc:Depends}, python3:any, python3-std-msgs, python3-tf2, python3-tf2-msgs, python3-tf2-ros, graphviz
Description: Robot OS tool for tf2 transform library second generation
 This package is part of Robot OS (ROS). tf2 is the second generation
 of the transform library, which lets the user keep track of multiple
 coordinate frames over time. tf2 maintains the relationship between
 coordinate frames in a tree structure buffered in time, and lets the
 user transform points, vectors, etc between any two coordinate frames
 at any desired point in time.
